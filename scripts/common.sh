#!/bin/bash

## @var     $1  String to trim
##
## @return      String trimmed
trim_spaces() {

    RETURN="$(echo -e "$1" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

}
