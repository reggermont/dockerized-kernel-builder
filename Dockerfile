FROM gcc:latest

LABEL maintainer="Eggermont Romain"

RUN apt-get update && apt-get install -y \
    bc \
    bison \
    build-essential:native \
    cpio \
    dirmngr \
    dpkg-dev \
    fakeroot \
    flex \
    gnupg \
    kmod \
    libelf-dev \
    libncurses5-dev \
    libssl-dev \
    perl \
    tar \
    wget

ARG USER

RUN useradd -ms /bin/bash ${USER}

USER ${USER}

WORKDIR /home/${USER}

COPY --chown=1000:1000 .config .env scripts/entrypoint.sh Makefile /home/${USER}/

ENTRYPOINT ["./entrypoint.sh"]
