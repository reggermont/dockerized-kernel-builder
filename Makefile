include .env

MAKEFLAGS += --no-print-directory

UNAME=$(shell uname -r)
_KERNEL_FOLDER=linux-${KERNEL_VERSION}

DOTENV_EXISTS=$(shell [ -f .env ] && echo 1 || echo 0 )
DOTCONFIG_EXISTS=$(shell [ -f .config ] && echo 1 || echo 0 )

KERNEL_LINK=https://cdn.kernel.org/pub/linux/kernel/v${KERNEL_MAJOR_VERSION}.x/linux-${KERNEL_VERSION}.tar.xz
KERNEL_FOLDER=linux-${KERNEL_VERSION}

make :
	@./scripts/kernelbuilder.sh $(version)

build :
	docker-compose up --build

config-import :
ifeq ($(DOTCONFIG_EXISTS),0)
	cp /boot/config-${UNAME} .config
else
	@echo 'custom .config imported, proceeding ...'
endif

dotenv-check :
ifeq ($(DOTENV_EXISTS),0)
	cp .env.dist .env
else
	@echo '.env exists, proceeding ...'
endif

kernel-build :
	cd $(KERNEL_FOLDER) && make deb-pkg -j"$(NUMBER_CORES_USE)" LOCALVERSION=-"$(ARCHITECTURE)" KDEB_PKGVERSION="$(NUMBER_CORES_USE)-$(DOCKER_USER)"

kernel-download :
	wget $(KERNEL_LINK) -O kernel.tar.xz -nv --https-only
	make kernel-untar

kernel-prepare :
	cp .config ./${KERNEL_FOLDER}
	cd ${KERNEL_FOLDER} && make olddefconfig \
		&& bash -c "./scripts/config -d CONFIG_MODULE_SIG_ALL -d CONFIG_MODULE_SIG_KEY -d CONFIG_SYSTEM_TRUSTED_KEYS -d CONFIG_DEBUG_INFO" \
		&& make clean

kernel-save :
	cp linux-headers* output/linux-headers-$(KERNEL_VERSION)-$(ARCHITECTURE)_$(DOCKER_USER).deb
	cp linux-image* output/linux-image-$(KERNEL_VERSION)-$(ARCHITECTURE)_$(DOCKER_USER).deb

kernel-untar :
	tar -xf kernel.tar.xz

prepare :
	@make dotenv-check
	@make config-import
	